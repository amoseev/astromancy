#! /bin/sh
set -euo pipefail

echo "start  eb deploy"
eb init astromancy  -r $AWS_DEFAULT_REGION  --platform "64bit Amazon Linux 2017.09 v2.8.3 running Docker 17.06.2-ce"
eb deploy
echo "finish eb deploy"