#! /bin/sh
set -euo pipefail

ARG1=${1:-$(git rev-parse HEAD)}

cd $(dirname $0)/../
app_dir=$(pwd)
deploy_dir=$app_dir/deployment

tag=$ARG1

rm -f Dockerrun.aws.json

#echo app_dir
#echo $app_dir
#echo ls-appdir
#ls -la $app_dir
#echo "app_dir/Dockerrun.aws.json"
#echo $app_dir/Dockerrun.aws.json

sed "s/:latest/:$tag/g" $app_dir/Dockerrun.aws.json.template >> $app_dir/Dockerrun.aws.json

#cat $app_dir/Dockerrun.aws.json