#!/bin/sh
cp $(dirname $0)/../Dockerrun.aws.json.template $(dirname $0)/../Dockerrun.aws.json

cp $(dirname $0)/../.env.dist $(dirname $0)/../.env
cp $(dirname $0)/../workspace/.env.example $(dirname $0)/../workspace/.env