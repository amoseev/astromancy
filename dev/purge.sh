#!/bin/sh
docker  exec -it -u www-data app.php-fpm rm -rf /var/log/app
docker  exec -it -u www-data app.php-fpm rm -rf /var/log/php

##!/bin/bash
# Delete all containers
#docker rm $(docker ps -a -q)
# Delete all images
#docker rmi $(docker images -q)