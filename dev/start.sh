#!/bin/sh
docker rm -f $(docker ps -a -q)
docker-compose up --build -d --force-recreate

docker  exec -it -u root app.nginx chown -R www-data:www-data /var/log/nginx
docker  exec -it -u root app.php-fpm chown -R www-data:www-data /srv/www/app
docker  exec -it -u root app.php-fpm mkdir -p /var/log/app
docker  exec -it -u root app.php-fpm chown -R www-data:www-data /var/log/app
docker  exec -it -u root app.php-fpm mkdir -p /var/log/php
docker  exec -it -u root app.php-fpm chown -R www-data:www-data /var/log/php

#generate key
docker exec -it -u www-data app.php-fpm php artisan key:generate
#----- application -------
# migrations
docker exec -it -u www-data app.php-fpm php artisan  doctrine:migrations:migrate