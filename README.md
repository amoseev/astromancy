#### Start
copy env files and update credentials:
```sh
$ dev/init.sh
```
Run docker-compose (composer install + migrate include):
```sh
$ dev/start.sh
```
