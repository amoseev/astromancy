<?php
declare(strict_types=1);

namespace Currency\Infrastructure\Repository;

use App\Infrastructure\Repository\AbstractRepository;
use Currency\Domain\Entity\Currency;

class CurrencyRepository extends AbstractRepository
{

    protected function getClassName(): string
    {
        return Currency::class;
    }
}